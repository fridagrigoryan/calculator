function myFunc(element) {
    var result = document.getElementById("textView");
    if (element.value === "+" && (
        getLastSymbol(result.value) ===  "." ||
        getLastSymbol(result.value) ===  "+" ||
        getLastSymbol(result.value) ===  "-" ||
        getLastSymbol(result.value) ===  "*" ||
        getLastSymbol(result.value) ===  "/"
    )) {
     return
    }
    if (element.value === "-" && (
        getLastSymbol(result.value) ===  "+" ||
        getLastSymbol(result.value) ===  "-" ||
        getLastSymbol(result.value) ===  "*" ||
        getLastSymbol(result.value) ===  "/"
    )) {
        return
    }
    if (element.value === "*" && (
        getLastSymbol(result.value) ===  "+" ||
        getLastSymbol(result.value) ===  "-" ||
        getLastSymbol(result.value) ===  "*" ||
        getLastSymbol(result.value) ===  "/"
    )) {
        return
    }
    if (element.value === "/" && (
        getLastSymbol(result.value) ===  "+" ||
        getLastSymbol(result.value) ===  "-" ||
        getLastSymbol(result.value) ===  "*" ||
        getLastSymbol(result.value) ===  "/"
    )) {
        return
    }
    if (element.value === "." && (
        getLastSymbol(result.value) ===  "." ||
        getLastSymbol(result.value) ===  "+" ||
        getLastSymbol(result.value) ===  "-" ||
        getLastSymbol(result.value) ===  "*" ||
        getLastSymbol(result.value) ===  "/"
    )) {
        return
    }

    if (result.value === "0") {
        result.value = element.value;
    } else  {
        result.value += element.value;
    }

    //console.log(getLastSymbol( document.getElementById("textView").value))
}

function equal() {
    let textView = document.getElementById("textView")
    var info = textView.value;
    if (info) {
        textView.value = eval(textView.value);
    }
}

function clean() {
    document.getElementById("textView").value = "0";
}

function getLastSymbol(text)
{
    return text.substr((text.length - 1))
}
